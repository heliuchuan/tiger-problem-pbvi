import numpy, decimal, sys, getopt
from pylab import *

class Tiger:
	def __init__(self, truth=.85, reward=10, punishment=-15, listencost =-1):
		self.truth = truth
		self.actions = ["Left", "Listen", "Right"]
		self.reward = reward
		self.punishment = punishment
		self.listencost = listencost

		rand = random()
		if (rand < .5):
			self.Location = "Left"
			self.WrongLocation = "Right"
		else: 
			self.Location = "Right"
			self.WrongLocation = "Left"

	def reset(self):
		rand = random()
		if (rand < .5):
			self.Location = "Left"
			self.WrongLocation = "Right"
		else: 
			self.Location = "Right"
			self.WrongLocation = "Left"

	def Reward(self, action, belief):
		if action == "Left":
			utility = self.reward*belief[0] + self.punishment*belief[1]
			return utility
		if action == "Right":
			utility = self.reward*belief[1] + self.punishment*belief[0]
			return utility
		if action =="Listen":
			return self.listencost

################################################################################################################################################################################################

class Counter(dict):
    """
    A counter keeps track of counts for a set of keys.

    The counter class is an extension of the standard python
    dictionary type.  It is specialized to have number values
    (integers or floats), and includes a handful of additional
    functions to ease the task of counting data.  In particular,
    all keys are defaulted to have value 0.  Using a dictionary:

    a = {}
    print a['test']

    would give an error, while the Counter class analogue:

    >>> a = Counter()
    >>> print a['test']
    0

    returns the default 0 value. Note that to reference a key
    that you know is contained in the counter,
    you can still use the dictionary syntax:

    >>> a = Counter()
    >>> a['test'] = 2
    >>> print a['test']
    2

    This is very useful for counting things without initializing their counts,
    see for example:

    >>> a['blah'] += 1
    >>> print a['blah']
    1

    The counter also includes additional functionality useful in implementing
    the classifiers for this assignment.  Two counters can be added,
    subtracted or multiplied together.  See below for details.  They can
    also be normalized and their total count and arg max can be extracted.
    """
    def __getitem__(self, idx):
        self.setdefault(idx, 0)
        return dict.__getitem__(self, idx)

    def incrementAll(self, keys, count):
        """
        Increments all elements of keys by the same count.

        >>> a = Counter()
        >>> a.incrementAll(['one','two', 'three'], 1)
        >>> a['one']
        1
        >>> a['two']
        1
        """
        for key in keys:
            self[key] += count

    def argMax(self):
        """
        Returns the key with the highest value.
        """
        if len(self.keys()) == 0: return None
        all = self.items()
        values = [x[1] for x in all]
        maxIndex = values.index(max(values))
        return all[maxIndex][0]

    def sortedKeys(self):
        """
        Returns a list of keys sorted by their values.  Keys
        with the highest values will appear first.

        >>> a = Counter()
        >>> a['first'] = -2
        >>> a['second'] = 4
        >>> a['third'] = 1
        >>> a.sortedKeys()
        ['second', 'third', 'first']
        """
        sortedItems = self.items()
        compare = lambda x, y:  sign(y[1] - x[1])
        sortedItems.sort(cmp=compare)
        return [x[0] for x in sortedItems]

    def totalCount(self):
        """
        Returns the sum of counts for all keys.
        """
        return sum(self.values())

    def normalize(self):
        """
        Edits the counter such that the total count of all
        keys sums to 1.  The ratio of counts for all keys
        will remain the same. Note that normalizing an empty
        Counter will result in an error.
        """
        total = float(self.totalCount())
        if total == 0: return
        for key in self.keys():
            self[key] = self[key] / total

    def divideAll(self, divisor):
        """
        Divides all counts by divisor
        """
        divisor = float(divisor)
        for key in self:
            self[key] /= divisor

    def copy(self):
        """
        Returns a copy of the counter
        """
        return Counter(dict.copy(self))

    def __mul__(self, y ):
        """
        Multiplying two counters gives the dot product of their vectors where
        each unique label is a vector element.

        >>> a = Counter()
        >>> b = Counter()
        >>> a['first'] = -2
        >>> a['second'] = 4
        >>> b['first'] = 3
        >>> b['second'] = 5
        >>> a['third'] = 1.5
        >>> a['fourth'] = 2.5
        >>> a * b
        14
        """
        sum = 0
        x = self
        if len(x) > len(y):
            x,y = y,x
        for key in x:
            if key not in y:
                continue
            sum += x[key] * y[key]
        return sum

    def __radd__(self, y):
        """
        Adding another counter to a counter increments the current counter
        by the values stored in the second counter.

        >>> a = Counter()
        >>> b = Counter()
        >>> a['first'] = -2
        >>> a['second'] = 4
        >>> b['first'] = 3
        >>> b['third'] = 1
        >>> a += b
        >>> a['first']
        1
        """
        for key, value in y.items():
            self[key] += value

    def __add__( self, y ):
        """
        Adding two counters gives a counter with the union of all keys and
        counts of the second added to counts of the first.

        >>> a = Counter()
        >>> b = Counter()
        >>> a['first'] = -2
        >>> a['second'] = 4
        >>> b['first'] = 3
        >>> b['third'] = 1
        >>> (a + b)['first']
        1
        """
        addend = Counter()
        for key in self:
            if key in y:
                addend[key] = self[key] + y[key]
            else:
                addend[key] = self[key]
        for key in y:
            if key in self:
                continue
            addend[key] = y[key]
        return addend

    def __sub__( self, y ):
        """
        Subtracting a counter from another gives a counter with the union of all keys and
        counts of the second subtracted from counts of the first.

        >>> a = Counter()
        >>> b = Counter()
        >>> a['first'] = -2
        >>> a['second'] = 4
        >>> b['first'] = 3
        >>> b['third'] = 1
        >>> (a - b)['first']
        -5
        """
        addend = Counter()
        for key in self:
            if key in y:
                addend[key] = self[key] - y[key]
            else:
                addend[key] = self[key]
        for key in y:
            if key in self:
                continue
            addend[key] = -1 * y[key]
        return addend

class prettyfloat(float):
    def __repr__(self):
        return "%0.3f" % self

################################################################################################################################################################################################

class PBVI:
	def __init__(self, Tiger, beliefs, discount = 1, iterations = 1):
		self.Tiger = Tiger
		self.discount = discount
		self.iterations = iterations
		
		self.alphas = Counter()
		self.policies = Counter()

		self.beliefs = []
		for belief in beliefs:
			self.beliefs.append((map(prettyfloat, belief)[0],map(prettyfloat, belief)[1]))
		
		for belief in self.beliefs:
			self.alphas[belief] = [0,0]
			self.policies[belief] = []



		index = 0

		while index < self.iterations:
			index = index + 1

			alphasCopy = self.alphas.copy()
			policiesCopy = self.policies.copy()

			for belief in self.beliefs:
				
					
				action = 'Left'
				alphavalues = Counter()
				for b, alpha in alphasCopy.items():
					alphavalues[b] = self.Tiger.Reward(action, belief) + self.discount * numpy.dot(self.beliefupdate(belief,'none'), alpha)
								
				LeftVal = alphavalues[alphavalues.argMax()]
				Leftbeliefkey = alphavalues.argMax()

				action = 'Right'
				alphavalues = Counter()
				for b, alpha in alphasCopy.items():
					alphavalues[b] = self.Tiger.Reward(action, belief) + self.discount * numpy.dot(self.beliefupdate(belief,'none'), alpha)
				RightVal = alphavalues[alphavalues.argMax()]
				Rightbeliefkey = alphavalues.argMax()

				action = 'Listen'
				alphavalues = Counter()
				alphavalues2 = Counter()
				for b, alpha in alphasCopy.items():
					alphavalues[b] = self.Leftexp(belief) * (self.Tiger.Reward(action, belief) + self.discount * numpy.dot(self.beliefupdate(belief,'Left'), alpha))
											

				for b, alpha in alphasCopy.items():
					alphavalues2[b] = self.Rightexp(belief) * (self.Tiger.Reward(action, belief) + self.discount * numpy.dot(self.beliefupdate(belief,'Right'), alpha))
				
				xx = alphavalues[alphavalues.argMax()]
				yy = alphavalues2[alphavalues2.argMax()]
				ListenVal = xx + yy
				
				ListenLeftbeliefkey = alphavalues.argMax()
				ListenRightbeliefkey = alphavalues2.argMax()

				find = queriedbelief
				indextarget = itarget
				if (belief[0]-find)<.0001 and (belief[0]-find) > -.0001 and index == indextarget:
					print"#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#"
					print "Belief: " + str(belief)
					print "Iteration: " + str(index)
					print "-------- Left Side --------"
					print "Pr(Hearing Left): " + str(self.Leftexp(belief))
					print "New Belief: " + str(map(prettyfloat, self.beliefupdate(belief,'Left')))
					print "Left Val: " + str(xx)
					print "Key: " + str(alphavalues.argMax())
					print "Policy: " + str(self.policies[ListenLeftbeliefkey])
					print "Alpha: " + str(alphasCopy[ListenLeftbeliefkey])
					print"-------- Right Side --------"
					print "Pr(Hearing Right): " + str(self.Rightexp(belief))
					print "New Belief: " + str(map(prettyfloat, self.beliefupdate(belief,'Right')))
					print "Right Val: " + str(yy)
					print "Key: " + str(alphavalues2.argMax())
					print "Policy: " + str(self.policies[ListenRightbeliefkey])
					print "Alpha: " + str(alphasCopy[ListenRightbeliefkey])
					print"-------- Values --------"
					print str(map(prettyfloat,[LeftVal, ListenVal, RightVal]))

				
				

				actionIndex = numpy.argmax([LeftVal, ListenVal, RightVal])


				
				if actionIndex == 0:
					policiesCopy[belief] = ['Left', self.policies[Leftbeliefkey]]
					self.alphas[belief] = self.alphaupdate(alphasCopy[Leftbeliefkey], 'Left')
				if actionIndex == 1:
					policiesCopy[belief] = ['Listen', self.policies[ListenLeftbeliefkey], self.policies[ListenRightbeliefkey]]
					leftalphavalue = self.Tiger.listencost + self.discount * (self.Tiger.truth * alphasCopy[ListenLeftbeliefkey][0]+ (1-self.Tiger.truth) * alphasCopy[ListenRightbeliefkey][0])
					rightalphavalue = self.Tiger.listencost + self.discount * ((1-self.Tiger.truth) * alphasCopy[ListenLeftbeliefkey][1] + self.Tiger.truth * alphasCopy[ListenRightbeliefkey][1])
					self.alphas[belief] = [leftalphavalue, rightalphavalue]

				if actionIndex == 2:
					policiesCopy[belief] = ['Right', self.policies[Rightbeliefkey]]
					self.alphas[belief] = self.alphaupdate(alphasCopy[Rightbeliefkey], 'Right')

				if (belief[0]-find)<.0001 and (belief[0]-find) > -.0001 and index == indextarget:
					print"-------- Alpha Update --------"
					print "L: " + str(leftalphavalue)
					print "R: " + str(rightalphavalue)
					print


			self.policies = policiesCopy



	def singleVal(self, dict):
		newdict = Counter()
		for key, value in dict.items():
			newdict[key[0]] = value
		return newdict

	def revDict(self, dict):
		newdict = Counter()
		for key, value in dict.items():
			newdict[value] = key
		return newdict

	def sortedDict(self, dict):
		return self.revDict(self.singleVal(dict)).sortedKeys()



	def alphaupdate(self, alphavec, action):
		return [(self.Tiger.Reward(action, [1,0]) + self.discount * (numpy.dot([.5,.5],alphavec))), (self.Tiger.Reward(action, [0,1]) + self.discount * (numpy.dot([.5,.5],alphavec)))] 

	def Leftexp(self, belief):
		return self.Tiger.truth*belief[0] + (1-self.Tiger.truth)*belief[1]

	def Rightexp(self, belief):
		return self.Tiger.truth*belief[1] + (1-self.Tiger.truth)*belief[0]


	def normalized(self, belief):
		if belief == (0,0):
			return (0,0)
		return (belief[0]/(belief[0]+belief[1]), belief[1]/(belief[0]+belief[1]))


	def beliefupdate (self, belief, rustle):
		if rustle == 'Left':
			newbelief = (belief[0] * self.Tiger.truth , belief[1] * (1-self.Tiger.truth))
			return self.normalized(newbelief)
		elif rustle == 'Right':
			newbelief = (belief[0] * (1-self.Tiger.truth) , belief[1] * self.Tiger.truth)
			return self.normalized(newbelief)
		else:
			self.Tiger.reset()
			return (.5,.5)

	def printPols(self):
		printed = []
		print "POLICIES"
		for i in range(len(self.policies.keys())):
			min = float('inf')
			anskey = ''
			ansval = ''
			for key, value in self.policies.items():
				if key not in printed:
					if key[0] < min:
						min = key[0]
						anskey = key
						ansval = value

			print str(map(prettyfloat, anskey)) + ": " + str(ansval)
			printed.append(anskey)

	def printAs(self):
		printed = []
		print "ALPHA VECTORS"
		for i in range(len(self.alphas.keys())):
			min = float('inf')
			anskey = ''
			ansval = ''
			for key, value in self.alphas.items():
				if key not in printed:
					if key[0] < min:
						min = key[0]
						anskey = key
						ansval = value

			print str(map(prettyfloat, anskey)) + ": " + str(map(prettyfloat, ansval))
			printed.append(anskey)

	def printVs(self):
		printed = []
		print "BELIEF POINT VALUES"
		for i in range(len(self.alphas.keys())):
			min = float('inf')
			anskey = ''
			ansval = ''
			for key, value in self.alphas.items():
				if key not in printed:
					if key[0] < min:
						min = key[0]
						anskey = key
						ansval = value
			bval = numpy.dot(anskey, ansval)
			print str(map(prettyfloat, anskey)) + ": " + str(prettyfloat(bval))
			printed.append(anskey)

	def f(self, x, alpha):
		y = alpha[0]*x + alpha[1]*(1-x)
		return y

	def f2(self, x):
		start = 0
		for key, value in self.alphas.items():
			if start == 0:
				maxvec = self.f(x, value)
				start = 1
			else:
				maxvec = numpy.maximum(maxvec, self.f(x, value))
		return maxvec

	def plotvec(self):
		plt.figure(1)
		t1 = numpy.arange(0.0, 1.0, 0.01)
		plt.subplot(211)
		for key, value in self.alphas.items():
			x = numpy.array(key[0])
			y = numpy.dot(key, value)
			plt.vlines(x,[0],y)
			xlabel('Pr(Tiger in the Left Door)')
			ylabel('Value of Belief State')
			title('Alpha Vectors dotted w/ Belief States')		
			plt.plot(t1, self.f(t1, value))
		plt.axhline(0, color='black', lw=1)

		plt.subplot(212)
		title('Max(Alpha Vectors dotted w/ Belief States)')
		xlabel('Pr(Tiger in the Left Door)')
		ylabel('Value of Belief State')
		plt.plot(t1, self.f2(t1))
		plt.show()

################################################################################################################################################################################################

def usage():
	print "-----------------~OPTIONS-----------------~"
	print "-h:		help menu"
	print "-i 'arg':	set number of iterations"
	print "-n 'arg':	set number of evenly spaced intial beliefs"
	print "-t 'arg':	set the probability that listen reveals the true door"
	print "-x 'arg':	set reward for choosing correct door"
	print "-y 'arg':	set punishment for choosing the door with the tiger"
	print "-d 'arg':	set discount rate"
	print "-c 'arg':	set cost of listening"
	print "-z 'arg':	debug a belief point (shows relevant calculations) at a given iteration given by -u"
	print "-u 'arg':	set iteration to debug, equals last iteration by default"
	print "-q 'arg':	query the best policy for a belief state"
	print "-p:		print the policies for all belief points"
	print "-a:		print the alpha vectors for all belief points"
	print "-v:		print the values for all belief points"
	print "-l:		plot the hyperplanes and the max of all hyperplanes"
	print ""
	print "-----------------~DEFAULT VALUES~-----------------~"
	print "Correct observation 	= .85"
	print "# of iterations 	= 3"
	print "# of belief points 	= 25"
	print "Reward 			= 10"
	print "Punishment 		= -15"
	print "Listencost 		= -1"
	print "Discount 		= 1"


################################################################################################################################################################################################


def main(argv):                                         
    
	space = 25
	iter = 3
	truth = .85
	_printP = 0
	_printA = 0
	_printV = 0
	_plot = 0
	discount = 1
	reward = 10
	punishment = -15
	listentax = -1
	global queriedbelief
	global itarget
	queriedbelief = -1
	itarget = iter
	querying = 0
	query = 0 

	try:                                
		opts, args = getopt.getopt(argv, "hi:n:t:pax:y:vld:c:z:u:q:", ["help", "iterations=", "beliefnumber=", "truth", "printpolicies", "printalphas", "beliefquery", "reward", "punishment", "iterquery", "printvalues", "plot", "discount", "listentax", "query"])
	except getopt.GetoptError:
		usage()
		sys.exit(2)                    
	for opt, arg in opts:             
		if opt in ("-h", "--help"):
			usage()
			sys.exit()
		elif opt in ("-i", "--iterations"):   
			iter = int(arg)
			itarget = iter                  
		elif opt in ("-n", "--beliefnumber"):                              
			space = float(arg)           
		elif opt in ("-t", "--truth"): 
			truth = float(arg)
		elif opt in ("-p", "printpolicies"): 
			_printP = 1
		elif opt in ("-a", "--printalphavectors"): 
			_printA = 1
		elif opt in ("-x", "--reward"): 
			reward = int(arg)
		elif opt in ("-y", "--punishment"): 
			punishment = int(arg)
		elif opt in ("-v", "--printvalues"): 
			_printV = 1
		elif opt in ("-l", "--plot"): 
			_plot = 1
		elif opt in ("-d", "--discount"): 
			discount = float(arg)
		elif opt in ("-c", "--listentax"): 
			listentax = float(arg)
		elif opt in ("-z", "--beliefquery"): 
			queriedbelief = float(arg)
		elif opt in ("-u", "--iterquery"): 
			itarget = int(arg)
		elif opt in ("-q", "--query"): 
			query = float(arg)
			querying = 1
		else:
			assert False, "unhandled option"


	interval = 1/float(space)

	beliefs = []
	for i in range(0,int(space)+1):
		b = (interval*i,1-(interval*i))
		beliefs.append(b)

	tigergame = Tiger(truth, reward, punishment, listentax)

	p = PBVI(tigergame, beliefs, discount, iter)

	if _printP:
		p.printPols()
	if _printA:
		p.printAs()
	if _printV:
		p.printVs()
	if _plot:
		p.plotvec()

	#Your belief query
	if querying:
		beliefstate = (query,1-query)
		max = -float('inf')
		amax = ''
		for beliefpoint, alphavec in p.alphas.items():
			if numpy.dot(beliefstate, alphavec) > max:
				max = numpy.dot(beliefstate, alphavec)
				policy = p.policies[beliefpoint]
				amax = alphavec
		print "Policy: " + str(policy)
		print "Alpha Vector: " + str(amax)

	
if __name__ == "__main__":
    main(sys.argv[1:])



